cd  /home/vague/robot_town/very-short-weather || exit 
#
#get_weather(){
#	# usage 
#	# 1 = url 
#	# 2 = output file
#	count_try=0
#	echo error > "$2"
#	while grep -i error "$2" -q ; do
#		echo retrieving >&2
#		lynx --dump "$1" > "$2"
#		count_try=$((count_try +1))
#		if [ "$count_try" -ge 3 ] ; then
#			exit
#		fi
#		sleep 1
#	done
#}
#get_weather "$1" "$2"

#wget -O - -U "Mozilla/5.0 (X11; Linux x86_64; rv:121.0) Gecko/20100101 Firefox/121.0" 'https://forecast.weather.gov/MapClick.php?lat=35.968&lon=-83.927&unit=0&lg=english&FcstType=text&TextType=1' | grep -e Today -e Tonight | sed -e 's/.*Today/Today/' | sed -e 's/.*Tonight/Tonight/' | sed -e 's/<\/b>//g' -e 's/<br>//g'

get_weather(){
	echo get weather "$1" "$2" >&2
	line=$(wget -O - -U "Mozilla/5.0 (X11; Linux x86_64; rv:121.0) Gecko/20100101 Firefox/121.0" "$1" | grep -e Today -e Tonight | sed -e 's/.*Today/Today/' | sed -e 's/.*Tonight/Tonight/' | sed -e 's/<\/b>//g' -e 's/<br>//g' )
	# telegram format cleanup
	line=$( echo $line | tr -cd '[:print:]\n[:blank:]'  | sed -e 's/\./\\./g' | sed -e 's/=/\\=/g' | sed -e 's/-/\\-/g'| sed -e 's/+/\\+/g' | sed -e 's/_/\\_/g'  )
	line=$(echo $line | tr '\n' ' ' )
	#echo $line 

	# cut out the weather sentence for just today and tonight
	today=$(echo $line | sed -e 's/Tonight.*//')
	tonight=$( echo $line | sed -e 's/.*Tonight/Tonight/' )
	(
	echo $(date)
	echo \\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\=\\= 
	echo \\* $today
	echo 
	echo \\* $tonight
	) > "$2"
	grep -q -i -e today -e tonight "$2" && cat "$2"
	#echo $line >"$2"
}

rm "$2" -v >&2
while ! grep -q -i -e today -e tonight "$2" 2>/dev/null ; do
	get_weather "$1" "$2"
	grep -q -i -e today -e tonight "$2" && continue
	sleep 5
	sleep $((RANDOM % 10))
done
#echo '```'
#cat "$2" 
#| tr -cd '[:print:]\n[:blank:]'  | sed -e 's/\./\\./g' | sed -e 's/=/\\=/g' | sed -e 's/+/\\+/g' | sed -e 's/_/\\_/g' 
#echo '```'
