cd  /home/vague/robot_town/long-weather || exit 

get_weather(){
	# usage 
	# 1 = url 
	# 2 = output file
	count_try=0
	echo error > "$2"
	while grep -i error "$2" -q ; do
		echo retrieving >&2
		lynx --dump "$1" > "$2"
		count_try=$((count_try +1))
		if [ "$count_try" -ge 3 ] ; then
			exit
		fi
		sleep 1
	done
}
get_weather "$1" "$2"

echo '```'
cat "$2" | tr -cd '[:print:]\n[:blank:]'  | sed -e 's/\./\\./g' | sed -e 's/=/\\=/g' | sed -e 's/+/\\+/g' | sed -e 's/_/\\_/g' 
echo '```'

