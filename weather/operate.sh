# https://github.com/fcambus/ansiweather


cd /home/vague/robot_town/weather || {
	exit 1
}

#bin=./ansiweather/ansiweather
#nix-shell -p jq bc --run "$bin -l jackson,tn,us -u imperial -F -a false -s true -d true"

cmd() {
	bin=./ansiweather/ansiweather
	#nix-shell -p jq bc --run "$bin $@"
	$bin $@
	echo $bin $@ >&2
}

(
header="$(date)"
echo ============================== 
echo $header
echo ============================== 
cmd "-l jackson,tn,us -u imperial -F -a false -s true -d true"
echo '   '
cmd "-l cookeville,tn,us -u imperial -F -a false -s true -d true"
echo '   '
cmd "-l kingston,tn,us -u imperial -F -a false -s true -d true"
) | sed -e 's/^\s//'
