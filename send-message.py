# vague
# utility to send a message to a channel. it works fine 
# usage: python3 $0 "your text here"

# https://www.freecodecamp.org/news/how-to-create-a-telegram-bot-using-python/
# https://github.com/eternnoir/pyTelegramBotAPI/blob/master/examples/timer_bot.py
# https://stackoverflow.com/questions/66612681/how-to-use-python-telegram-bot-to-send-messages-to-a-telegram-channel
#    they say just use the http request >_>
# https://github.com/eternnoir/pyTelegramBotAPI/blob/master/examples/multibot/main.py

import os
import telebot
import time, threading

BOT_API_KEY='YOUR KEY HERE'
CHATID='YOUR CHAT ID HERE (prefix with '-', not @ or -100 or whatever)'

bot = telebot.TeleBot(BOT_API_KEY)

def testmsg(CHATID,bot, msg):
    bot.send_message(CHATID, msg)

def senddoc(CHATID,bot,docpath):
    # TODO
    bot.send_document(CHATID, 'url to image')

#bot.infinity_polling()

if __name__ == '__main__':
    threading.Thread(target=bot.infinity_polling, name='bot_infinity_polling', daemon=True).start()
    time.sleep(0.5)

    if len(sys.argv) == 1:
        print('no arg')
        quit(1)
    msg=sys.argv[1]
    print('send msg ', msg)
    testmsg(CHATID,bot,msg)
#    while True:
        ##schedule.run_pending()
        #time.sleep(1)
